import string

PRIORITIES = string.ascii_lowercase + string.ascii_uppercase


def priority(item: str) -> int:
    return PRIORITIES.find(item) + 1


def find_common_item_priority(input_lines: list[str]) -> int:
    result = ""
    common_items = input_lines[0]
    for input_line in input_lines[1:]:
        result = ""
        for item in input_line:
            if item in common_items:
                result += item
        common_items = result
    return priority(result[0])


def day03() -> int:
    result: int = 0
    group_of_three: list[str] = []
    with open("day03-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            group_of_three.append(line)
            if len(group_of_three) == 3:
                result += find_common_item_priority(group_of_three)
                group_of_three = []
    return result


if __name__ == "__main__":
    print(day03())
