def day01() -> int:
    max_calories: int = 0
    current_calories: int = 0
    with open("day01-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            try:
                current_calories += int(line)
            except ValueError:
                if current_calories > max_calories:
                    max_calories = current_calories
                current_calories = 0
    return max_calories


if __name__ == "__main__":
    print(f"Result: {day01():,d}")
