def day02() -> tuple[list[int], int]:
    top_three_calories: list[int] = []
    current_calories: int = 0
    with open("day01-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            try:
                current_calories += int(line)
            except ValueError:
                top_three_calories.append(current_calories)
                top_three_calories.sort(reverse=True)
                del top_three_calories[3:]
                current_calories = 0
    return top_three_calories, sum(top_three_calories)


if __name__ == "__main__":
    top_three, total = day02()
    print(f"Top three: {top_three}; Total Calories: {total:,d}")
