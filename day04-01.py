def parse_input_line(input_line: str) -> tuple[int, int, int, int]:
    elf1, elf2 = input_line.split(",")
    elf1_start, elf1_end = elf1.split("-")
    elf2_start, elf2_end = elf2.split("-")
    return int(elf1_start), int(elf1_end), int(elf2_start), int(elf2_end)


def day04() -> int:
    result: int = 0
    with open("day04-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            elf1_start, elf1_end, elf2_start, elf2_end = parse_input_line(line)
            if ((elf1_start <= elf2_start) and (elf1_end >= elf2_end)) or (
                (elf1_start >= elf2_start) and (elf1_end <= elf2_end)
            ):
                result += 1
    return result


if __name__ == "__main__":
    print(day04())
