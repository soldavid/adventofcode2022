opponent: dict[str,int] = {
    "A": 1, # Rock
    "B": 2, # Paper
    "C": 3, # Scissors
}

response: dict[str,int] = {
    "X": 1, # Rock
    "Y": 2, # Paper
    "Z": 3, # Scissors
}

outcome : dict[tuple[str, str], int] = {
    ("A", "X"): 3, # Rock     vs. Rock     = Draw
    ("A", "Y"): 6, # Rock     vs. Paper    = Win
    ("A", "Z"): 0, # Rock     vs. Scissors = Lose
    ("B", "X"): 0, # Paper    vs. Rock     = Lose
    ("B", "Y"): 3, # Paper    vs. Paper    = Draw
    ("B", "Z"): 6, # Paper    vs. Scissors = Win
    ("C", "X"): 6, # Scissors vs. Rock     = Win
    ("C", "Y"): 0, # Scissors vs. Paper    = Lose
    ("C", "Z"): 3, # Scissors vs. Scissors = Draw
}

def day02() -> int:
    result: int = 0
    with open("day02-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            input_opponent, input_response = line.strip("\n").split(" ")
            result += response[input_response] + outcome[input_opponent, input_response]
    return result


if __name__ == "__main__":
    print(f"Result: {day02():,d}")
