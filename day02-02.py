opponent: dict[str,int] = {
    "A": 1, # Rock
    "B": 2, # Paper
    "C": 3, # Scissors
}

wanted_result: dict[str,int] = {
    "X": 0, # Lose
    "Y": 3, # Draw
    "Z": 6, # Win
}

response : dict[tuple[str, str], int] = {
    ("A", "X"): 3, # Rock     and Lose then vs. Scissors
    ("A", "Y"): 1, # Rock     and Draw then vs. Rock
    ("A", "Z"): 2, # Rock     and Win  then vs. Paper
    ("B", "X"): 1, # Paper    and Lose then vs. Rock
    ("B", "Y"): 2, # Paper    and Draw then vs. Paper
    ("B", "Z"): 3, # Paper    and Win  then vs. Scissors
    ("C", "X"): 2, # Scissors and Lose then vs. Paper
    ("C", "Y"): 3, # Scissors and Draw then vs. Scissors
    ("C", "Z"): 1, # Scissors and Win  then vs. Rock
}

def day02() -> int:
    result: int = 0
    with open("day02-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            input_opponent, input_result = line.strip("\n").split(" ")
            result += wanted_result[input_result] + response[input_opponent, input_result]
    return result


if __name__ == "__main__":
    print(f"Result: {day02():,d}")
