import re

stacks: dict[int, list[str]] = {
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: [],
    9: [],
}


def parse_stack_line(input_line: str) -> None:
    for stack, char_position in enumerate(range(1, 34, 4), 1):
        if input_line[char_position] != " ":
            stacks[stack].append(input_line[char_position])


def parse_movement(input_line: str) -> None:
    parsed_numbers = re.findall(r"\d+", input_line)
    number_movements = int(parsed_numbers[0])
    from_stack = int(parsed_numbers[1])
    to_stack = int(parsed_numbers[2])
    moved_items = stacks[from_stack][0:number_movements]
    stacks[from_stack] = stacks[from_stack][number_movements:]
    stacks[to_stack] = moved_items + stacks[to_stack]


def day05() -> str:
    result: str = ""
    with open("day05-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            if line[0] == "[":
                parse_stack_line(line)
            elif line[0:4] == "move":
                parse_movement(line)
    # pprint(stacks)
    for stack_number in range(1, 10):
        result += stacks[stack_number][0]
    return result


if __name__ == "__main__":
    print(day05())
