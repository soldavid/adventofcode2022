import string

PRIORITIES = string.ascii_lowercase + string.ascii_uppercase


def priority(item: str) -> int:
    return PRIORITIES.find(item) + 1


def find_item_priority(rucksack: str) -> int:
    half_position = len(rucksack) // 2
    first_compartment = rucksack[:half_position]
    second_compartment = rucksack[half_position:]
    for item in first_compartment:
        if item in second_compartment:
            return priority(item)
    return 0


def day03() -> int:
    result: int = 0
    with open("day03-01-input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            result += find_item_priority(line)
    return result


if __name__ == "__main__":
    print(day03())
